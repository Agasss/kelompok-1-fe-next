import { useState, useEffect } from "react";
import * as React from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import Box from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import style from "../../styles/gamedetails.module.css";
import {useRouter} from "next/router"
import Navbar from "../../components/navbar/index";
import Footer from "../../components/Footer/index";

const GameDetail = () => {
  const [gamelist, setGamelist] = useState([]);
  const router = useRouter()
  const {id} = router.query

  useEffect(() => {
    if(!id) {
      return;
    }
    const handleFetch = () => {
      axios.get(`http://localhost:5000/games/detail/${id}`)
      .then((res) => {
        setGamelist(res.data);
      });
    };
    handleFetch();
  }, [id])

  return (
    <>
    <Navbar/>
    <div className={style.containergd}>
      <Card sx={{ display: "flex", width: 1, height: "450px" }}>
        <Box sx={{ maxWidth: 1 / 2 }}>
          <CardContent>
            <Typography
              gutterBottom
              variant="h2"
              component="div"
              sx={{ width: 1, ml: 1 }}
              style={{ fontWeight: "bold" }}
            >
              {gamelist?.result?.name}
            </Typography>
            <Typography
              gutterBottom
              variant="h6"
              component="div"
              sx={{ width: 0.9, ml: 1 }}
            >
              {gamelist?.result?.detail}
            </Typography>
          </CardContent>
          <CardActions style={{ marginBottom: "20px", marginTop: "30px" }}>
            <Button
              size="md"
              style={{
                fontWeight: "bold",
                backgroundColor: "black",
                color: "white",
                width: "40%",
              }}
              sx={{ ml: 1 }}
            >
              <a href="/rps">PLAY GAME</a>
            </Button>
          </CardActions>
        </Box>
        <CardMedia
          sx={{
            maxWidth: 1 / 2,
            backgroundSize: "cover",
            height: "inherit",
            width: "inherit",
          }}
          image={gamelist?.result?.image}
          title="game image"
          style={{ objectFit: "contain", borderRadius: "10px 0 0 10px" }}
        />
      </Card>
    </div>
    <Footer/>
    </>
  );
};

export default GameDetail;