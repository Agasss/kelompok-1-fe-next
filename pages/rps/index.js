import Play from "../../components/RPS/play";
import { useRouter } from "next/router";
import { useEffect } from "react";

const RPS = () => {
  const router = useRouter()

  useEffect(() => {
    const token = localStorage.getItem("token");
    if(token==null){router.replace("/gamedetails/1")}
  }, []);
  return <Play />;
};

export default RPS;
