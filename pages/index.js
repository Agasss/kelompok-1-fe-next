import * as React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import {
  Card,
  Box,
  CardActions,
  Typography,
  CardContent,
  CardMedia,
  Button,
  Grid,
} from "@mui/material";
import Navbar from "../components/navbar/index";
import Footer from "../components/Footer/index";
import imgBp from "../public/assets/Home_Page_Big_Picture.jpg";
import imgRps from "../public/assets/RPS.png";
import imgNba from "../public/assets/NBA2K23.jpg";
import imgVal from "../public/assets/VALORANT.jpg";
import landingStyles from "../styles/landing.module.css";
import { Container } from "react-bootstrap";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";

const LandingPage = () => {
  const [gamelist, setGamelist] = useState([]);
  const router = useRouter()
  
  const handleFetch = () => {
    axios.get("http://localhost:5000/games").then((res) => {
      setGamelist(res);
      console.log(res.data.result);
    });
  };

  useEffect(() => {
    handleFetch();
    const token = localStorage.getItem("token");
    if(token!==null){router.replace("/home")}
  }, []);

  return (
    <React.Fragment>
      <Navbar />
      <div className={landingStyles.containerlp}>
        <Card
          sx={{
            display: "flex",
            width: 1,
            borderRadius: "10px",
          }}
        >
          <Box sx={{ width: "50%" }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h2"
                component="div"
                sx={{ width: 1 }}
                style={{ fontWeight: "bold" }}
              >
                Welcome!!
              </Typography>
              <p>
                Feel free to play games on our website after you are logged in
                !!
              </p>
            </CardContent>
            <CardActions style={{ marginBottom: "20px", marginTop: "30px" }}>
              <Button
                size="md"
                style={{
                  frontWeight: "bold",
                  backgroundColor: "black",
                  color: "white",
                  marginLeft: "10px",
                }}
                href="/about"
              >
                about us
              </Button>
            </CardActions>
          </Box>
          <Image src={imgBp} className={landingStyles.img1} />
        </Card>
      </div>
      <div className={landingStyles.containerlp2}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Card sx={{ width: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{
                    fontWeight: "bold",
                    textOverflow: "ellipsis",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                  }}
                  component="div"
                >
                  Rock, Paper & Scissors
                </Typography>
              </CardContent>
              <Image alt="" src={imgRps} className={landingStyles.img2} />
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card sx={{ maxWidth: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{ fontWeight: "bold" }}
                  component="div"
                >
                  NBA 2K23
                </Typography>
              </CardContent>
              <Image
                component="img"
                alt=""
                src={imgNba}
                className={landingStyles.img3}
              ></Image>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card sx={{ maxWidth: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{ fontWeight: "bold" }}
                  component="div"
                >
                  VALORANT
                </Typography>
              </CardContent>
              <Image
                component="img"
                alt=""
                src={imgVal}
                className={landingStyles.img4}
              ></Image>
            </Card>
          </Grid>
        </Grid>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default LandingPage;
