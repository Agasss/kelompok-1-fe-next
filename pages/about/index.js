import * as React from "react";
// import "./about.css";
import AboutUs from "../../public/assets/about_us_1.png";
import Image from "next/image";
import style from "../../styles/about.module.css";
import Navbar from "../../components/navbar/index";
import Footer from "../../components/Footer/index";

const About = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className={style.container}>
        <div className={style.aboutcontainer}>
          <div className={style.container35}>
            <div className={style.h1}>
              <h1 className={style.title}>About Us</h1>
            </div>

            <p className={style.paragraf}>
              This website is an online games platform which you can play
              anywhere, anytime. With more than 30+ online games, and a
              leaderboard feature on every game to make it more competitive.
            </p>
            <p>
              This is a project made by 5 incredible engineers to fulfill the
              requirements for Full Stack Web Development Bootcamp by Binar
              Academy (wave 27).
            </p>
            <p>Meet the team!</p>
            <ul>
              <li className={style.anggota}>Agastya Tristyan Dhaniswara</li>
              <li className={style.anggota}>Dzaky Adla Hikmatiyar</li>
              <li className={style.anggota}>Fajar Akvianto Putra</li>
              <li className={style.anggota}>Muhammad Haiqal</li>
              <li className={style.anggota}>Salma Khoerunisa</li>
            </ul>
            <button className={style.contactus}>Contact Us</button>
          </div>
          <div className={style.container40}>
            <Image className={style.aboutus} src={AboutUs} alt="about us" />
          </div>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default About;
